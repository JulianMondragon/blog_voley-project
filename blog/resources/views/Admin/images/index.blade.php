@extends('Admin.template.main')

@section('title' , 'Imagenes')
@section('subtitle' , '\ Listado de Imagenes')


@section('inner')
	<br>
	<div class="row gtr-50 gtr-uniform">
		@foreach($images as $image)
				<div class="col-4">
					<div class="panel-default">
						<div class="panel-body">
							<img src="{{ asset('images/articles/') .'/'. $image->name }}"  style="width: 100%;  height: 100%; ">
						</div>
						<!-- {!! $Characters = substr($image->article->title, 0, 4); !!} -->
						<div class="panel-footer">
							<h3><a href="{{ url('articles/index1' . '?title=' . $image->article->title ) }}">{{ $image->article->title }}</a></h3>
							
						</div>
					</div>
				</div>		
		@endforeach
	</div>
@endsection

 
