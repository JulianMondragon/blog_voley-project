<!DOCTYPE html>
<html lang="es">
	<head>
		<title> @yield('title')| Panel de Admin</title>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no" />
		<link rel="stylesheet" href="{{ asset('plugins/assets/css/main.css') }}" />
		<link rel="stylesheet" href="{{ asset('plugins/chosen/chosen.css') }}" />
		<link rel="stylesheet" href="{{ asset('plugins/trumbowyg/ui/trumbowyg.css') }}" />
	</head>
	<body class="is-preload">

		<!-- Wrapper -->
			<div id="wrapper">

				<!-- Main -->
					<div id="main">

						<div class="inner">
							<!-- Header -->
							@include('Admin.template.partials.header')
							@include('Admin.template.partials.errors')
							@include('flash::message')
							@yield('inner')
							
						</div>

					</div>

				<!-- Sidebar -->
					@include('Admin.template.partials.navigation')
					</div>
					

			</div>

		<!-- Scripts  -->
		    <script src="{{ asset('plugins/bootstrap/css/bootstrap.css') }}"></script>
			<script src="{{ asset('plugins/bootstrap/js/bootstrap.js') }}"></script>
			<script src="{{ asset('plugins/assets/js/jquery.min.js') }}"></script>
			<script src="{{ asset('plugins/assets/js/browser.min.js') }}"></script>
			<script src="{{ asset('plugins/assets/js/breakpoints.min.js') }}"></script>
			<script src="{{ asset('plugins/assets/js/util.js') }}"></script>
			<script src="{{ asset('plugins/assets/js/main.js') }}"></script>
			<script src="{{ asset('plugins/chosen/chosen.jquery.js') }}"></script>			
			<script src="{{ asset('plugins/trumbowyg/trumbowyg.js') }}"></script>

			@yield('js')

	</body>
</html>

</html>