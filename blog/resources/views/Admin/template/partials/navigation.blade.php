<div id="sidebar">
						<div class="inner">

							<!-- Search -->
								<section id="search" class="alt">
									<!-- Search -->
							<!-- buscador de articulos-->
						        {!! Form::open(['route' => 'Articles.index1', 'method' => 'GET', 'class' => 'navbar-form pull-right']) !!}
						            <div class="form-group">
						                {!! Form::text('title', null, ['class' => 'form-control', 'placeholder' => 'Buscar Articulo...', 'aria-describedby' => 'search']) !!}
						            </div>
						        {!! Form::close() !!}
								</section>

							<!-- Menu -->
								<nav id="menu">
									<header class="major">
										<h2>Menu</h2>
									</header>
									<ul>
									@if(Auth::user())
										<li>
											<span class="opener">{{ Auth::user()->name }}</span>
											<ul>
												<li>
													<form action="{{ route('logout') }}" method="POST" >
													     {{ csrf_field() }}
													     <button class="btn btn-danger" ><i class="fa fa-sign-in" > logout</i></button>
											        </form>
											    </li>
											</ul>
										</li>
								    @endif
										<li><a href="{{ route('homepage') }}">Home</a></li>
									@if(Auth::user())
										<li><a href="{{ url('Admin/Users') }}">Usuarios</a></li>
										<li><a href="{{ url('Admin/Categories') }}">Categorias</a></li>
									@endif
										<li>
											<span class="opener">Articulos</span>
											<ul>
										@if(Auth::user())
										      <li><a href="{{ route('articles.create') }}">Crear articulo </a></li>
											  <li><a href="{{ route('articles.index') }}">Listado de articulos</a></li>
										@endif
												<!-- se debe quitar esta linea despues -->
												<li><a href="{{ route('Articles.index1') }}">Vista de los articulos </a></li>
											</ul>
										</li>
										<li><a href="{{ route('images.index') }}">Imagenes</a></li>
										@if(Auth::user())
										<li><a href="{{ url('Admin/Tags') }}">Tags</a></li>
										@endif
									</ul>
								</nav>
							<!-- Section -->
								<section>
									<header class="major">
										<h2>Contactanos</h2>
									</header>
									<p>El Instituto del Deporte y la Recreación del Estado de Querétaro (INDEREQ) y PRODEPORTE, con el aval de la Asociación Estatal de Querétaro de Voleibol y de la Comisión Permanente de Voleibol de playa del Estado de Querétaro, invitan a los jugadores y jugadoras de voleibol de playa, y al público en general, a participar en Circuito Estatal de Voleibol de Playa PRODEPORTE-2000</p>
									<ul class="contact">
										<li class="fa-envelope-o"><a href="#">octaviomedinag@hotmail.com</a></li>
										<li class="fa-phone">(442) 128-0509</li>
										<li class="fa-map-marker"><h5><font style="vertical-align: inherit;"><font class="goog-text-highlight" style="vertical-align: inherit;">PARQUE QUERETARO 2000</font></font></h5>Blvd. Bernanrdo Quintana SN<br />
										   Col. Villas del Parque <br>
									       Querétaro, Qro.
									    </li>
									    <li class="fa-map-marker"><h5><font style="vertical-align: inherit;"><font class="goog-text-highlight" style="vertical-align: inherit;">AUDITORIO ARTEAGA</font></font></h5>Av. Universidad no. 13 Pte.<br />
									    	Centro Histórico <br />
									    	Queretaro, Qro.
									    </li>
									</ul>
								</section>
							<!-- Section -->
								<section>
									<header class="major">
										<h2>Nuestros servicios</h2>
									</header>
									<div class="mini-posts">
										<article>
											<a href="#" class="image"><img src="{{ asset('plugins/images/pic04.jpg') }}" alt="" /></a>
											<h4><font style="vertical-align: inherit;"><font class="goog-text-highlight" style="vertical-align: inherit;">PREPARACIÓN FÍSICA</font></font></h4>
											<p>Desarrollamos tu máximo potencial deportivo a través de ejercicios específicos de tu deporte, complementándolo con un plan de alimentación adecuado y rutinas alternas que te harán llegar al siguiente nivel</p>
										</article>
										<article>
											<a href="#" class="image"><img src="{{ asset('plugins/images/pic02.jpg') }}" alt="" /></a>
											<h4><font style="vertical-align: inherit;"><font class="goog-text-highlight" style="vertical-align: inherit;">ANTROPOMETRIA DEPORTIVA</font></font></h4>
											<p>Vamos a conocer tu estructura y composición corporal para determinar y explotar al máximo tus mejores habilidades deportivas.</p>
										</article>
										<article>
											<a href="#" class="image"><img src="{{ asset('plugins/images/pic03.jpg') }}" alt="" /></a>
											<h4><font style="vertical-align: inherit;"><font class="goog-text-highlight" style="vertical-align: inherit;">NEUROENTRENAMIENTO DEPORTIVO</font></font></h4>
											<p>Tu mente puede llevar a tu cuerpo a niveles que jamas imaginarias que puedes llegar</p>
										</article>
									</div>
									<ul class="actions">
										<li><a href="{{ route('homepage') }}" class="button">Mas sobre nosotros</a></li>
									</ul>
								</section>
							<!-- Footer -->
								<footer id="footer">
									<p class="copyright">&copy; PRODEPORTE-2000. All rights reserved. <a href="https://unsplash.com">Unsplash</a>. Design: <a href="">Julian Mondragon</a>.</p>
								</footer>

						</div>
