<!-- Header -->
<header id="header">
	<a href="{{ route('home') }}" class="logo"><strong>Liga De Voleibol De Playa</strong>  @yield('subtitle')</a>

	<!-- icosnos de faceboook ..... -->
    <ul class="icons">			
		<li><a href="#" class="icon fa-twitter"><span class="label">Twitter</span></a></li>
		<li>
			<a href="{{ url('https://www.facebook.com/ProdeporteVoleibolPlaya/') }}" class="icon fa-facebook"><span class="label">Facebook</span></a>
		</li>
		<li><a href="#" class="icon fa-snapchat-ghost"><span class="label">Snapchat</span></a></li>
		<li><a href="#" class="icon fa-instagram"><span class="label">Instagram</span></a></li>	
	@if(Auth::user())
		<li>
			<span class="opener">{{ Auth::user()->name }}</span>
		</li>
		<li>
			<form action="{{ route('logout') }}" method="POST" >
			     {{ csrf_field() }}	
			     <button class="button small" ><i class="fa fa-sign-in" > logout</i></button>
	        </form>
	    </li>
	@else
	 	 <li>
	 	 	<a href="{{ route('login') }}">
		 		<h4>
		 			<i  href="" class="fa fa-sign-in" > login</i>
		 		</h4>
			</a> 											
     	</li>
	@endif			
	</ul>
	
</header>