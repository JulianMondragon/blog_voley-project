<!-- Section 1 -->
<section>
	<header class="major">
		<h2>Diciplinas</h2>
	</header>
	<div class="features">
		<article>
			<span class="icon fa-dribbble"></span>
			<div class="content">
				<h3>VOLEIBOL DE SALA</h3>
				<p>Entrena con nostros esta clásica disciplina de deporte en equipo y participa en torneos nacionales e internacionales.</p>
			</div>
		</article>
		<article>
			<span class=" icon fa-dribbble"></span>

			<div class="content">
				<h3>VOLEIBOL DE PLAYA</h3>
				<p>Ven a entrenar con nosotros en las mejores instalaciones de arena del estado,  en esta disciplina Olímpica que tanto gusta a los espectadores.</p>
			</div>
		</article>
		<article>
			<span class="icon fa-futbol-o"></span>
			<div class="content">
				<h3>FUTBOL DE PLAYA</h3>
				<p>Prueba esta variante del futbol donde desarrollarás tu agilidad, velocidad y precisión de tiro.</p>
			</div>
		</article>
		<article>
			<span class="icon fa-signal"></span>
			<div class="content">
				<h3>Sed magna finibus</h3>
				<p>Aenean ornare velit lacus, ac varius enim lorem ullamcorper dolore. Proin aliquam facilisis ante interdum. Sed nulla amet lorem feugiat tempus aliquam.</p>
			</div>
		</article>
	</div>
</section>
