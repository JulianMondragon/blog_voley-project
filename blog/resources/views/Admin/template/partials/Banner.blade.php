<!-- Banner -->
<section id="banner">
	<div class="content">
		<header>
			<h1>PRODEPORTE<br />
			2000</h1>
			<p>CIRCUITO ESTATAL DE VOLEIBOL DE PLAYA II</p>
		</header>
		<p style="align-content: center; ">LA LIGA CEDVP, ES UNA ORGANIZACIÓN DEPORTIVA INTERESCOLAR QUE PRETENDE DESARROLLAR EN SUS PARTICIPANTES EL SENTIDO DE RESPONSABILIDAD, EL ESFUERZO Y DISCIPLINA A TRAVÉS DE LA COMPETENCIA ASÍ COMO EL APOYO Y EJEMPLO DE LOS ADULTOS EN SU COMPORTAMIENTO..</p>
		<ul class="actions">
			<li><a href="#" class="button big">Learn More</a></li>
		</ul>
	</div>
	<span class="image object">
		<img src="{{ asset('plugins/images/pic012.jpg') }}" alt="" />
	</span>
</section>
