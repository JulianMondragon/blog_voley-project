@extends('Admin.template.main')


@section('title' , 'Login')

@section('subtitle' , ' \ Login')

@section('inner')
    {!! Form::open(['route' => 'login', 'method' => 'POST']) !!}
        <div class="form-group">
            <br>
            {!! Form::label('email', 'Correo electronico') !!}
            {!! Form::text('email' , null , ['class' => 'form-control' , 'placeholder' => 'correo_electronico@ejemplo.com' , 'required'] ) !!}
        </div>

        <div class="form-group">
            <br>
                {!! Form::label ('password' , 'Contraseña' )!!}
                {!! Form::password ('password' ,  ['class' => 'form-control' , 'placeholder' => '********' , 'required'] )!!}
        </div>

        <div class="form-group">
            <br>
                {!! Form::submit('Ingresar' , ['class' => 'btn btn-primary'])!!}
        </div>
    {!! Form::close() !!}
@endsection