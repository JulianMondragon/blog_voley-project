@extends('Admin.template.main')

@section('subtitle', '\  Editar categoria ' . $category->name)

@section('inner')

{!! Form::open(['route' => ['Categories.update', $category], 'method' => 'PUT']) !!}

<div class="form-group">
	<br>
	{!!	Form::label ('name' , 'Nombre' )!!}
	{!!	Form::text ('name' , $category->name , ['class' => 'form-control' , 'placeholder' => 'Nombre Completo' , 'required'] )!!}
	
</div>

<div class="form-group">
	<br>
	{!!	Form::submit ('Guardar' , ['class' => 'btn btn-primary'])!!}
	
</div>

{!! Form::close() !!} 

@endsection