@extends('Admin.template.main')

@section('title' , 'Categorias')
@section('subtitle' , '\ Agregar categoria')


@section('inner')

{!! Form::open(['route' => 'Categories.store' , 'method' => 'POST']) !!} 
<br>
<div class="form-group">
	{!!	Form::label('name' , 'Nombre')!!}
	{!! Form::text('name' , null, ['class' => 'form-control', 'placeholder' => 'Nombre de la categoria' , 'required']) !!}
	
</div>
<br>
<div class="form-group">
	{!!	Form::submit('Registar' , ['class' => 'btn btn-primary'])!!}
	
</div>

{!! Form::close() !!}
@endsection