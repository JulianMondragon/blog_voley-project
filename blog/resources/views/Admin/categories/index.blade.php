@extends('Admin.template.main')

@section('title' , 'Categorias')
@section('subtitle' , '\ Listado de categorias')


@section('inner')
<br>
<a href="{{ route('Categories.create') }}" class="button primary">
	<i  href="" class="fa fa-newspaper-o " > Agregar categoria</i>
</a>
<hr>
<table>
	<thead>
		<tr>
			<th>ID</th>
			<th>Nombre</th>
			<th>Accion</th>

		</tr>
	</thead>
	<tbody>
	    @foreach($categories as $category)
			<tr>
			 <td>{{ $category->id }}</td>
			 <td>{{ $category->name }}</td>
			 <td>

			 	<a href="{{ route('Categories.edit', $category->id) }}" class="button">
							<i  href="" class="fa fa-pencil-square-o" aria-hidden="true"> Editar</i>
				</a>

				<a href="{{ route('Categories.destroy' , $category->id) }}" onclick="return confirm('¿seguro que deseas eliminar este usuario?')" class="button primary">
				             <i  href="" class="fa fa-trash" aria-hidden="true"> Borrar</i>
			    </a>
			 </td>
		    </tr>
		@endforeach
	</tbody>
</table>
@endsection
