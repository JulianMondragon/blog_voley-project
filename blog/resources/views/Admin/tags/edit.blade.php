@extends('Admin.template.main')

@section('subtitle', '\  Editar tag ' . $tag->name)

@section('inner')

{!! Form::open(['route' => ['Tags.update', $tag], 'method' => 'PUT']) !!}

<div class="form-group">
	<br>
	{!!	Form::label ('name' , 'Nombre' )!!}
	{!!	Form::text ('name' , $tag->name , ['class' => 'form-control' , 'placeholder' => 'Nombre Completo' , 'required'] )!!}
	
</div>

<div class="form-group">
	<br>
	{!!	Form::submit ('Guardar' , ['class' => 'btn btn-primary'])!!}
	
</div>

{!! Form::close() !!} 

@endsection