@extends('Admin.template.main')

@section('title' , 'Tags')
@section('subtitle' , '\ Listado de Tags')


@section('inner')
<!-- Boton para agregar un tag -->
<br>
<a href="{{ route('Tags.create') }}" class="button primary">
	<i  href="" class="fa fa-hashtag " > Agregar Tag</i>
</a>
<!-- buscador de tagas-->
{!! Form::open(['route' => 'Tags.index', 'method' => 'GET', 'class' => 'navbar-form pull-right']) !!}
	<div class="form-group">
		{!! Form::text('name', null, ['class' => 'form-control', 'placeholder' => 'Buscar Tag...', 'aria-describedby' => 'search']) !!}
		<!--<span class="input-group-addon" id="search"><span class="fa fa-search"></span></span>-->
	</div>
{!! Form::close() !!}
<!--Tabla de los tags existentes -->
<hr>
<table>
	<thead>
		<tr>
			<th>ID</th>
			<th>Nombre</th>
			<th>Accion</th>

		</tr>
	</thead>
	<tbody>
		 @foreach($tags as $tag)
			<tr>
			 <td>{{ $tag->id }}</td>
			 <td>{{ $tag->name }}</td>
			 <td>

			 	<a href="{{ route('Tags.edit', $tag->id) }}" class="button">
							<i  href="" class="fa fa-pencil-square-o" aria-hidden="true"> Editar</i>
				</a>

				<a href="{{ route('Tags.destroy' , $tag->id) }}" onclick="return confirm('¿seguro que deseas eliminar este usuario?')" class="button primary">
				             <i  href="" class="fa fa-trash" aria-hidden="true"> Borrar</i>
			    </a>
			 </td>
		    </tr>
		@endforeach
	</tbody>
</table>
<div class="text-center">
	{!! $tags->render() !!}
</div>
@endsection
