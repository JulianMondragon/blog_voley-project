@extends('Admin.template.main')

@section('title' , 'Agregar Tags')
@section('subtitle' , '\ Agregar Tag')


@section('inner')

{!! Form::open(['route' => 'Tags.store' , 'method' => 'POST']) !!} 
<br>
<div class="form-group">
	{!!	Form::label('name' , 'Nombre')!!}
	{!! Form::text('name' , null, ['class' => 'form-control', 'placeholder' => 'Nombre del tag' , 'required']) !!}
	
</div>
<br>
<div class="form-group">
	{!!	Form::submit('Registar' , ['class' => 'btn btn-primary'])!!}
	
</div>

{!! Form::close() !!}
@endsection