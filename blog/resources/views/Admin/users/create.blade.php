@extends('Admin.template.main')

@section('subtitle',' / Crear usario')

@section('inner')
   

   
	{!! Form::open(['route' => 'Users.store', 'method' => 'POST']) !!}

	<div class="form-group">
		<br>
		{!!	Form::label ('name' , 'Nombre' )!!}
		{!!	Form::text ('name' , null , ['class' => 'form-control' , 'placeholder' => 'Nombre Completo' , 'required'] )!!}
		
	</div>

	<div class="form-group">
		<br>
		{!!	Form::label ('email' , 'Correo Electronico' )!!}
		{!!	Form::email ('email' , null , ['class' => 'form-control' , 'placeholder' => 'example@gmail.com' , 'required'] )!!}
		
	</div>

	<div class="form-group">
		<br>
		{!!	Form::label ('password' , 'Contraseña' )!!}
		{!!	Form::password ('password' ,  ['class' => 'form-control' , 'placeholder' => '********' , 'required'] )!!}
		
	</div>

	<div class="form-group">
		<br>
		{!!	Form::label ('type' , 'Tipo' )!!}
		{!!	Form::select ('type' ,  ['member' => 'Miembro' , 'admin' => 'Administrador'], null , ['class' => 'form-control', 'placeholder' => 'Seleccione el tipo de usuario...', 'required']) !!}
		
	</div>

	<div class="form-group">
		<br>
		{!!	Form::submit ('Resgistar' , ['class' => 'btn btn-primary'])!!}
		
	</div>

	{!! Form::close() !!} 

@endsection

	
