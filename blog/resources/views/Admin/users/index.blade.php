@extends('Admin.template.main')

@section('subtitle', '\  Lista Usuarios')

@section('inner')
<br>
<a href="{{ route('Users.create') }}" class="button primary">
	<i  href="" class="fa fa-user-plus " > Agregar</i>
</a>
<br>
<br>
 <table class="table-wrapper">
 	<thead >
 		<tr>
 			<th>ID</th>
 			<th>Nombre</th>
 			<th>Email</th>
 			<th>Tipo</th>
 			<th>Accion</th>
 		</tr>
 	</thead>
 	<tbody>
 		@foreach($users as $user)
	 		<tr>
	 			 <td>{{ $user->id  }}</td>
		         <td>{{ $user->name }}</td>
		         <td>{{ $user->email }}</td>
		         <td>
		         	@If( $user->type == "member")
                         <span class="alert alert-primary">{{ $user->type }}</span>
		         	@else
                          <span class="label label-success">{{ $user->type }}</span>
		         	@endif
		         	
		         </td>
		         <td> 
		            <a href="{{ route('Users.edit', $user->id) }}" class="button ">
							<i  href="" class="fa fa-pencil-square-o" aria-hidden="true"> Editar</i>
					</a>		
					<a href="{{ route('Users.destroy' , $user->id) }}" onclick="return confirm('¿seguro que deseas eliminar este usuario?')" class="button primary">
							<i  href="{{ route('Users.destroy' , $user->id) }}" class="fa fa-trash" aria-hidden="true"> Borrar</i>
					</a>					
		         </td>
	 		</tr>	         
 		@endforeach
 	</tbody>
 </table>
 <div class="" align="center">
 	 {!! $users->render() !!}
 </div>


@endsection