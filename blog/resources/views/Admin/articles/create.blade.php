@extends('Admin.template.main')

@section('title' , 'Articulos')
@section('subtitle' , '\ Agregar Articulo')


@section('inner')
<br>
{!! Form::open(['route' => 'articles.store', 'method' => 'POST', 'files' => true])!!}
    <div class="form-group">
    	{!! Form::label('title' , 'Titulo') !!}
    	{!! Form::text('title' , null , [ 'class' => 'form-control', 'placeholder' => 'Titulo del articulo ...', 'required'] ) !!}
    </div>
    <br>
    <div class="form-group">
    	{!! Form::label('category_id' , 'Categoria') !!}
    	{!! Form::select('category_id', $categories, null,['class' => 'form-control', 'required']) !!}
    </div>
    <br>
    <div class="form-group">
    	{!! Form::label('content' , 'Contenido') !!}
    	{!! Form::textarea('content', null, ['class' => 'form-control textarea-content' , 'placeholder' => 'escriba el contenido del articulo ...']) !!}
    </div>
    <br>
    <div class="form-group">
    	{!! Form::label('tags' , 'Tags') !!}
    	{!! Form::select('tags[]', $tags, null,['class' => 'form-control select-tag' , 'multiple', 'required']) !!}
    </div>
    <br>
    <div class="form-group">
    	{!! Form::label('image' , 'Imagen') !!}
    	{!! Form::file('image')!!}
    </div>
    <br>
    <div class="form-group">
    	{!! Form::submit('Agregar Articulo' , ['class' => 'btn btn-primary'])!!}
    </div>

{!! Form::close() !!}
@endsection

@section('js')
	<script>
		$('.select-tag').chosen({
			placeholder_text_multiple: 'Selecione un maximo de 3 tags',
			max_selected_options: 3,
			search_contains: true,
			no_results_text: 'No se encontraron los tags'
		});

		$('.select-category').chosen({
            placeholder_text_single: 'Seleciona una categoria ...'
		});

		$('.textarea-content').trumbowyg();
	</script>

@endsection
