@extends('Admin.template.main')

@section('title', 'Articulos')
@section('subtitle', ' \ listado de Articulos')

@section('inner')
<br>
<!-- buscador de articulos-->
{!! Form::open(['route' => 'Articles.index1', 'method' => 'GET', 'class' => 'navbar-form pull-right']) !!}
	<div class="form-group">
		{!! Form::text('title', null, ['class' => 'form-control', 'placeholder' => 'Buscar Articulo...', 'aria-describedby' => 'search']) !!}
		<!--<span class="input-group-addon" id="search"><span class="fa fa-search"></span></span>-->
	</div>
{!! Form::close() !!}
  @foreach($articles as $article)

  <br>
		<article>
			<h3>{{ $article->title}} | <i class="fa fa-clock-o"></i>
      {{ $article->created_at->diffForHumans() }}</h3>
			@foreach($article->images as $image)
				<img  style="width: 100%" src="{{ asset('images/articles/') .'/'. $image->name }}" alt="" />

			 @endforeach

			<hr>
			{{$article->content}}
			<hr>
			{{$article->user->name}} | {{ $article->category->name}} |

			@foreach($article->tags as $tag)
			 	{{ $tag->name }}
			 @endforeach
			 <hr>
		</article>
  @endforeach
<div class="text-center" align="center">
  {{ $articles->render()}}
</div>
@endsection
