@extends('Admin.template.main')
   
@section('title', 'Articulos')
@section('subtitle', ' \ listado de Articulos')    

@section('inner')
<br>
<a href="{{ route('articles.create') }}" class="button primary">
	<i  href="" class="fa fa-id-card " > Agregar Articulo</i>
</a>
<!-- buscador de tagas-->
{!! Form::open(['route' => 'articles.index', 'method' => 'GET', 'class' => 'navbar-form pull-right']) !!}
	<div class="form-group">
		{!! Form::text('title', null, ['class' => 'form-control', 'placeholder' => 'Buscar Articulo...', 'aria-describedby' => 'search']) !!}
		<!--<span class="input-group-addon" id="search"><span class="fa fa-search"></span></span>-->
	</div>
{!! Form::close() !!}
<hr>
<table>
	<thead>
		<tr>
			<th>ID</th>
			<th>Nombre</th>
			<th>Categoria</th>
			<th>Usuario</th>
			<th>Accion</th>
			
		</tr>
	</thead>
	<tbody>		
	    @foreach($articles as $article)
			<tr>
			 <td>{{ $article->id }}</td>
			 <td>{{ $article->title }}</td>
			 <td>{{ $article->category->name }}</td>
			 <td>{{ $article->user->name }}</td>
			 <td>

			 	<a href="{{ route('articles.edit', $article->id) }}" class="button">
							<i  href="" class="fa fa-pencil-square-o" aria-hidden="true"> Editar</i>
				</a>	

				<a href="{{ route('articles.destroy' , $article->id) }}" onclick="return confirm('¿seguro que deseas eliminar este articulo?')" class="button primary">
				             <i  href="" class="fa fa-trash" aria-hidden="true"> Borrar</i>
			    </a>
			 </td>	
		    </tr>
		@endforeach
	</tbody>
</table>
@endsection
