@extends('Admin.template.main')

@section('title', 'Home')

@section('inner')
<!--header-->
<br>
<!-- buscador de articulos-->
{!! Form::open(['route' => 'Articles.index1', 'method' => 'GET', 'class' => 'navbar-form pull-right']) !!}
    <div class="form-group">
        {!! Form::text('title', null, ['class' => 'form-control', 'placeholder' => 'Buscar Articulo...', 'aria-describedby' => 'search']) !!}
    </div>
{!! Form::close() !!}
<header class="major">
    <h3>Ultimos Articulos </h3>
</header>
<div class="row">
  <!-- articulos-->
  <div class="col-9 col-12-small">
    <div class="row">
      <!-- Section -->
          <section>
              <div class="posts">
                  @foreach($articles as $article)
                      <article>
                          <h3>{{ $article->title}}</h3>
                          @foreach($article->images as $image)
                            <a href="{{ url('articles/index1' . '?title=' . $article->title )}}">
                              <img  style="width: 100%" src="{{ asset('images/articles/') .'/'. $image->name }}" alt="" />
                            </a>
                          @endforeach
                          <p>{{$article->content}}</p>
                          {{ $article->category->name}} |
                          @foreach($article->tags as $tag)
                              {{ $tag->name }}
                          @endforeach
                           | <i class="fa fa-clock-o"></i>
                           {{ $article->created_at->diffForHumans() }}
                          <ul class="actions">
                              <li>
                                  <br>
                                  <a href="{{ url('articles/index1' . '?title=' . $article->title ) }}" class="button">Ver al articulo </a>
                              </li>
                          </ul>
                      </article>
                  @endforeach
              </div>
              <div class="" align="center">
                 {!! $articles->render() !!}
              </div>
          </section>
    </div>
  </div>
  <!-- contenido de recuadros de categorias y tags-->
  <div class="col-2 col-12-medium">
    @include('client.partials.aside')
  </div>
</div>

@endsection
