  <!-- Categorias-->
<div class="box">
    <header class="major">
      <h4>Categorias</h4>
    </header>
      <ul class="alt">
        @foreach($categories as $category)
         <li>
            <a href="{{ route('seach.category', $category->name )}}" class="icon fa-newspaper-o">
               <span class="label"> </span>{{ $category->name }}
           </a>
         </li>
        @endforeach
        <li></li>
      </ul>
</div>
<br>
  <!-- Tags-->
<div class="box">
    <header class="major">
      <h4>Tags</h4>
    </header>
      <ul class="alt">
        @foreach($tags as $tag)
          <li><a href="{{ route('seach.tag', $tag->name )}}" class="icon fa-hashtag"> <span class="label"> </span>{{$tag->name }}</a></li>
        @endforeach
        <li></li>
      </ul>
</div>
