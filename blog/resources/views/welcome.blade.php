@extends('Admin.template.main')
   
@section('title', 'Home')   

@section('inner')
    <!-- Banner -->
    @include('Admin.template.partials.Banner')
    <!-- Section 1 -->
    @include('Admin.template.partials.section1')
    <!-- Section 2 -->
    @include('Admin.template.partials.section2')
@endsection
