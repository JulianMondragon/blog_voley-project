<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Article;
use App\Category;
use App\Tag;
use Carbon\Carbon;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(Request $request)
    {
       Carbon::setLocale('es');
       $categories = Category::orderby('name' , 'DESC')->paginate(4);
       $tags = Tag::orderby('name' , 'DESC')->get();
        $articles =  Article::Search($request->title)->orderby('id' , 'DESC')->paginate(6);
        $articles->each(function($articles){
            $articles->category;
            $articles->user;
            $articles->images;
        });
       return view('client.lastsArticles')
              ->with('articles' , $articles)
              ->with('categories' , $categories)
              ->with('tags' , $tags);
    }

    public function seachCategory($name)
    {
      Carbon::setLocale('es');
      $categories = Category::orderby('name' , 'DESC')->paginate(4);
      $tags = Tag::orderby('name' , 'DESC')->get();
      $category = Category::SearchCategory($name)->first();
      $articles = $category->articles()->paginate(4);
      $articles->each(function($articles){
          $articles->category;
          $articles->user;
          $articles->images;
      });
     return view('client.lastsArticles')
     ->with('articles' , $articles)
     ->with('categories' , $categories)
     ->with('tags' , $tags);
    }

    public function seachTag($name)
    {
      Carbon::setLocale('es');
      $categories = Category::orderby('name' , 'DESC')->paginate(4);
      $tags = Tag::orderby('name' , 'DESC')->get();
      $tag = Tag::SearchTag($name)->first();
      $articles = $tag->articles()->paginate(4);
      $articles->each(function($articles){
          $articles->category;
          $articles->user;
          $articles->images;
      });
     return view('client.lastsArticles')
     ->with('articles' , $articles)
     ->with('categories' , $categories)
     ->with('tags' , $tags);
    }

    public function viewArticle($slug)
    {
        
    }
}
