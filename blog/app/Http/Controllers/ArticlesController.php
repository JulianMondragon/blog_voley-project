<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Laracasts\Flash\Flash;
use App\Category;
use App\Tag;
use App\Article;
use App\Image;
use App\Http\Requests\ArticleRequest;
use Carbon\Carbon;


class ArticlesController extends Controller
{

   	public function index(Request $request)
   	{

   		$articles =  Article::Search($request->title)->orderby('id' , 'ASC')->paginate(5);
        $articles->each(function($articles){
            $articles->category;
            $articles->user;
            $articles->images;
        });
   		return view('Admin.articles.index')
            ->with('articles' , $articles);
   	}

    public function showArticles(Request $request)
    {
        Carbon::setLocale('es');
        $articles =  Article::Search($request->title)->orderby('id' , 'ASC')->paginate(1);
        $articles->each(function($articles){
            $articles->category;
            $articles->user;
            $articles->images;
        });
        return view('Admin.articles.index1')
            ->with('articles' , $articles);
    }


    public function create()
    {
    	$categories = Category::orderBy('name', 'ASC')->pluck('name' , 'id');
    	$tags = Tag::orderby('name', 'ASC')->pluck('name' , 'id');
    	return view('Admin.articles.create')
    	         ->with('categories' , $categories)
    	         ->with('tags' , $tags);
    }

    public function store(ArticleRequest $request)
    {
    	//Manipulacion de archivos
    	if( $request->file('image'))
    	{
	    	$file = $request->file('image');
	    	$name = 'blogprodeporte_' . time() . '.' . $file->getClientOriginalExtension();
	    	$path = public_path() . '/images/articles';
	    	$file->move($path , $name);
    	}

    	$article = new Article($request->all());
    	$article->user_id = \Auth::user()->id;
    	$article->save();

    	$article->tags()->sync($request->tags);

    	$image = new Image();
    	$image->name = $name;
    	$image->article()->associate($article);
    	$image->save();

    	Flash::success('Se ha guardado el articulo ' . $article->title . ' de forma satisfactoria!');

    	return redirect()->route('articles.index');
    }

    public function edit($id)
    {
        $article = Article::find($id);
        $article->category;
        $categories = Category::orderBy('name', 'DESC')->pluck('name', 'id');
        $tags = Tag::orderBy('name', 'DESC')->pluck('name', 'id');

        $my_tags = $article->tags->pluck('id')->ToArray();

        return view('Admin.articles.edit')
        ->with('categories', $categories)
        ->with('article', $article)
        ->with('tags', $tags)
        ->with('my_tags', $my_tags);
    }

    public function update(Request $request, $id)
    {
        $article = Article::find($id);
        $article->fill($request->all());
        $article->save();
        $article->tags()->sync($request->tags);
        Flash::warning('Se ha editado el articulo: '.$article->title);
        return redirect()->route('articles.index');
    }

    public function destroy($id)
    {
        $article = Article::find($id);
        $article->delete();

        flash::error('Se ha borrado el articulo: ' . $article->title . ' de forma exitosa!');
        return redirect()->route('articles.index');
    }


}
