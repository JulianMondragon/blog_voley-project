<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Laracasts\Flash\Flash;
use App\Http\Requests\UserRequest;

class UsersController extends Controller
{
   

    //index
    public function index()
    {
    	$users = User::orderby('id' , 'ASC')->paginate(5);
    	return view('Admin.users.index')->with('users', $users);
    }

    //create
    public function create()
    {
    	return view('Admin.users.create');
    }

    //store
    public function store(UserRequest $request)
    {
    	$user = new User($request->all());
    	$user->password = bcrypt($request->password);
    	$user->save();
        Flash::success("Se ha registrado " . $user->name . " de forma exitosa");
    	return redirect()->route('Users.index');
    }

    public function destroy($id)
    { 
        $user = User::Find($id);      
        Flash::error('Se ha elimino el usuario de forma exitosa, adios  '  . $user->name);
        $user->delete(); 
        return redirect()->route('Users.index');

    }

    public function edit($id)
    {
        $user = User::Find($id);
        return view('Admin.users.edit')->with('user' , $user);
    }

    public function update(request $request , $id)
    {
        $user = User::Find($id);
        $user->name = $request->name;
        $user->email = $request->email;
        $user->type = $request->type;
        $user->save();

        Flash::warning('El usuario ' . $user->name . ' ha sido editado con exito');
        return redirect()->route('Users.index'); 
    }

}
