<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\TagRequest;
use App\Tag;
use Laracasts\Flash\Flash;

class TagsController extends Controller
{
    public function index(request $request)
    {
        $tags = Tag::Search($request->name)->orderBy('id' , 'DESC')->paginate(5);
    	return view('Admin.tags.index')->with('tags', $tags);

    }

    public function create()
    {
    	return view('Admin.tags.create');

    }

    public function edit($id)
    {
        $tag = Tag::find($id);
    	return view('Admin.tags.edit')->with('tag' , $tag); 

    }

    public function store(TagRequest $request)
    {
        $tag = new Tag($request->all());
        $tag->save();

        Flash::success('El tag ' . $tag->name . ' ha sido creada con exito!');
        return redirect()->route('Tags.index');
    }

    public function destroy($id)
    {
        $tag = Tag::find($id);
        $tag->delete();

         Flash::success('El tag ' . $tag->name . ' ha sido eliminado con exito!');
        return redirect()->route('Tags.index');
    }

    public function update(request $request , $id)
    {
        $tag = Tag::Find($id);
        $tag->fill($request->all());
        $tag->save();

        Flash::warning('El Tag ' . $tag->name . ' ha sido editado con exito');
        return redirect()->route('Tags.index'); 
    }
}
