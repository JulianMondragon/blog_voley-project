<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\CategoryRequest;
use App\Category;
use Laracasts\Flash\Flash;

class CategoriesController extends Controller
{
	public function index()
    {
    	$categories = Category::orderby('id' , 'ASC')->paginate(5);
    	return view('Admin.categories.index')->with('categories' , $categories);
    }

    public function store(CategoryRequest $request)
    {
    	$category = new Category($request->all());
    	$category->save();

    	Flash::success('La categoria ' . $category->name . ' ha sido guardada con exito');
    	return redirect()->route('Categories.index');
    	
    }


    public function create()
    {
    	return view('Admin.categories.create');
    }

     public function edit($id)
    {
        $category = Category::Find($id);
        return view('Admin.categories.edit')->with('category' , $category);
    }

     public function destroy($id)
    { 
        $category = Category::Find($id);      
        Flash::error('Se ha elimino la categoria ' . $category->name . ' de forma exitosa');
        $category->delete(); 
        return redirect()->route('Categories.index');
    }
      public function update(request $request , $id)
    {
        $category = Category::Find($id);
        $category->fill($request->all());
        $category->save();

        Flash::warning('La categoria ' . $category->name . ' ha sido editada con exito');
        return redirect()->route('Categories.index'); 
    }
}
