<?php

namespace App\Http\ViewComposers;


use Illuminate\Contracts\View\View;
use App\Category;
use App\Tag;

/**
 * esta clase regresa en la vista las categorias y tags en la vista principal
 */
class AsideComposer{

  public function compose(View $view)
  {
    $categories = Category::all();
    $view->with('categorias', $categories);
  }
}
