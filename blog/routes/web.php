<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



// Authentication Routes...
Route::get('Admin/auth/login', 'Auth\LoginController@showLoginForm')->name('login');
Route::post('Admin/auth/login', 'Auth\LoginController@login');
Route::post('Admin/auth/logout', 'Auth\LoginController@logout')->name('logout');

//RUTAS DEL MODULO DE ADMINISTRACION
Route::group(['prefix' => 'Admin' , 'middleware' => ['auth']], function()
{
    //Usuarios
    Route::resource('Users' , 'UsersController');
    Route::get('User/{id}/destroy' , [
    	'uses' => 'UsersController@destroy',
    	'as'  => 'Users.destroy'

    ]);
    //Categorias
    Route::resource('Categories' , 'CategoriesController');
    Route::get('Category/{id}/destroy', [
        'uses' => 'CategoriesController@destroy',
        'as' => 'Categories.destroy'
    ]);
    //Tags
    Route::resource('Tags' , 'TagsController');
    Route::get('Tags/{id}/destroy', [
        'uses' => 'TagsController@destroy',
        'as' => 'Tags.destroy'
    ]);
    //Articulos
    Route::resource('articles' , 'ArticlesController');
    Route::get('article/{id}/destroy', [
        'uses' => 'ArticlesController@destroy',
        'as' => 'articles.destroy'
    ]);


});



//RUTAS DEL MODULO DE USUARIOS
   Route::get('/', 'HomeController@index')->name('home');

   Route::get('homepage', function() {
       return view('welcome');
   })->name('homepage');


   //busqueda de categorias
   Route::get('categories/{name}' , [
     'uses' => 'HomeController@seachCategory',
     'as' => 'seach.category'
   ]);

   //busqueda de tags
   Route::get('tags/{name}' , [
     'uses' => 'HomeController@seachTag',
     'as' => 'seach.tag'
   ]);

    //listado de articulos
    Route::get('articles/index1', [
        'uses' => 'ArticlesController@showArticles',
        'as' => 'Articles.index1'
   ]);

    //Imagenes
    Route::get('images.index', [
        'uses'  => 'ImagesController@show',
        'as'    => 'images.index'

    ]);
